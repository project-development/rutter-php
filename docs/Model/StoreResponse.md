# StoreResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**store** | [**\Everpress\Rutter\Model\Store**](Store.md) |  | [optional] 
**connection** | [**\Everpress\Rutter\Model\Connection**](Connection.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

