# OrdersResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**orders** | [**\Everpress\Rutter\Model\Order[]**](Order.md) |  | [optional] 
**requestId** | **string** |  | [optional] 
**connection** | [**\Everpress\Rutter\Model\Connection**](Connection.md) |  | [optional] 
**nextCursor** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

