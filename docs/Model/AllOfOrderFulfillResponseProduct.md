# AllOfOrderFulfillResponseProduct

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**carrier** | **string** | Carrier company, ex \&quot;USPS\&quot;, \&quot;Fedex\&quot;, \&quot;UPS\&quot;, \&quot;Canada Post\&quot;, etc. Can be any string | 
**trackingNumber** | **string** |  | 
**trackingUrl** | **string** |  | [optional] 
**service** | **string** | Carrier shipping service, e.g. \&quot;Priority Mail\&quot;, \&quot;2 Day delivery\&quot;, etc. Can be any string | [optional] 
**locationId** | **string** | Required if the line item can be fulfilled from multiple locations. This field is required for Shopify. | [optional] 
**lineItems** | [**\Everpress\Rutter\Model\OrderFulfillLineItems[]**](OrderFulfillLineItems.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

