# CredentialResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**credential** | [**\Everpress\Rutter\Model\OAuth**](OAuth.md) |  | [optional] 
**connection** | [**\Everpress\Rutter\Model\Connection**](Connection.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

