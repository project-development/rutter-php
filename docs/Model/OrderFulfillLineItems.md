# OrderFulfillLineItems

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**lineItemId** | **string** | Line item id that can be matched in this order | [optional] 
**quantity** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

