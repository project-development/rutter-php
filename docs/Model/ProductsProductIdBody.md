# ProductsProductIdBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**product** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

