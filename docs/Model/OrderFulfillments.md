# OrderFulfillments

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**orderId** | **string** |  | [optional] 
**carrier** | **string** |  | [optional] 
**service** | **string** |  | [optional] 
**trackingNumber** | **string** |  | [optional] 
**trackingUrl** | **string** |  | [optional] 
**lineItems** | [**\Everpress\Rutter\Model\LineItem[]**](LineItem.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

