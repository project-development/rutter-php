# LineItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional] 
**platformId** | **string** |  | [optional] 
**productId** | **string** |  | [optional] 
**variantId** | **string** |  | [optional] 
**unitCost** | **float** |  | [optional] 
**price** | **float** |  | [optional] 
**isoCurrencyCode** | **string** |  | [optional] 
**quantity** | **int** |  | [optional] 
**sku** | **string** |  | [optional] 
**title** | **string** |  | [optional] 
**vendor** | **string** |  | [optional] 
**name** | **string** |  | [optional] 
**totalDiscount** | **float** |  | [optional] 
**fulfillmentService** | **string** |  | [optional] 
**inventoryManagement** | **string** |  | [optional] 
**requiresShipping** | **bool** |  | [optional] 
**barcode** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

