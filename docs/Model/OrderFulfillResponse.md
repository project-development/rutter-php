# OrderFulfillResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**product** | [**AllOfOrderFulfillResponseProduct**](AllOfOrderFulfillResponseProduct.md) |  | [optional] 
**connection** | [**\Everpress\Rutter\Model\Connection**](Connection.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

