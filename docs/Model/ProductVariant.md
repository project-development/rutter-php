# ProductVariant

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional] 
**productId** | **string** |  | [optional] 
**title** | **string** |  | [optional] 
**price** | **int** |  | [optional] 
**isoCurrencyCode** | **string** |  | [optional] 
**sku** | **string** |  | [optional] 
**fulfillmentService** | **string** |  | [optional] 
**inventoryManagement** | **string** |  | [optional] 
**barcode** | **string** |  | [optional] 
**unitCost** | **string** |  | [optional] 
**weight** | [**\Everpress\Rutter\Model\ProductVariantWeight**](ProductVariantWeight.md) |  | [optional] 
**optionValues** | [**\Everpress\Rutter\Model\ProductVariantOptionValues[]**](ProductVariantOptionValues.md) |  | [optional] 
**images** | [**\Everpress\Rutter\Model\ProductImages[]**](ProductImages.md) |  | [optional] 
**inventory** | [**\Everpress\Rutter\Model\ProductInventory**](ProductInventory.md) |  | [optional] 
**requiresShipping** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

