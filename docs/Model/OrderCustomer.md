# OrderCustomer

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional] 
**email** | **string** |  | [optional] 
**firstName** | **string** |  | [optional] 
**lastName** | **string** |  | [optional] 
**ordersCount** | **string** |  | [optional] 
**verifiedEmail** | **bool** |  | [optional] 
**phone** | **string** |  | [optional] 
**tags** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

