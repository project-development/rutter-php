# JobResponseResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**httpStatus** | **int** |  | [optional] 
**body** | [**\Everpress\Rutter\Model\JobResponseResponseBody**](JobResponseResponseBody.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

