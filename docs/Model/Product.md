# Product

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** | Unique identifier for product | [optional] 
**platformId** | **string** | The identifier of the product from the platform | [optional] 
**type** | **string** | The identifier of the product from the platform | [optional] 
**name** | **string** |  | [optional] 
**description** | **string** |  | [optional] 
**images** | [**\Everpress\Rutter\Model\ProductImages[]**](ProductImages.md) |  | [optional] 
**status** | **string** |  | [optional] 
**variants** | [**\Everpress\Rutter\Model\ProductVariant[]**](ProductVariant.md) |  | [optional] 
**tags** | **string[]** |  | [optional] 
**createdAt** | [**\DateTime**](\DateTime.md) |  | [optional] 
**updatedAt** | **string** |  | [optional] 
**productUrl** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

