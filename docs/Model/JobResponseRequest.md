# JobResponseRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**url** | **string** |  | [optional] 
**method** | **string** |  | [optional] 
**body** | [**\Everpress\Rutter\Model\JobResponseRequestBody**](JobResponseRequestBody.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

