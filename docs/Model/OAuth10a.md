# OAuth10a

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **string** |  | [optional] 
**oauthConsumerKey** | **string** |  | [optional] 
**oauthConsumerSecret** | **string** |  | [optional] 
**oauthToken** | **string** |  | [optional] 
**oauthTokenSecret** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

