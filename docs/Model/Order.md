# Order

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional] 
**platformId** | **string** |  | [optional] 
**orderNumber** | **string** |  | [optional] 
**status** | **string** |  | [optional] 
**paymentStatus** | **string** |  | [optional] 
**fulfillmentStatus** | **string** |  | [optional] 
**fulfillments** | [**\Everpress\Rutter\Model\OrderFulfillments[]**](OrderFulfillments.md) |  | [optional] 
**lineItems** | [**\Everpress\Rutter\Model\LineItem[]**](LineItem.md) |  | [optional] 
**refunds** | [**\Everpress\Rutter\Model\OrderRefunds[]**](OrderRefunds.md) |  | [optional] 
**billingAddress** | [**\Everpress\Rutter\Model\Address**](Address.md) |  | [optional] 
**shippingAddress** | [**\Everpress\Rutter\Model\Address**](Address.md) |  | [optional] 
**customer** | [**\Everpress\Rutter\Model\OrderCustomer**](OrderCustomer.md) |  | [optional] 
**totalShipping** | **float** |  | [optional] 
**totalDiscount** | **int** |  | [optional] 
**totalTax** | **float** |  | [optional] 
**totalPrice** | **float** |  | [optional] 
**isoCurrencyCode** | **string** |  | [optional] 
**createdAt** | [**\DateTime**](\DateTime.md) |  | [optional] 
**updatedAt** | [**\DateTime**](\DateTime.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

