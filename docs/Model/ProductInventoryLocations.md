# ProductInventoryLocations

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional] 
**name** | **string** |  | [optional] 
**address1** | **string** |  | [optional] 
**address2** | **string** |  | [optional] 
**city** | **string** |  | [optional] 
**postalCode** | **string** |  | [optional] 
**region** | **string** |  | [optional] 
**country** | **string** |  | [optional] 
**updatedAt** | **string** |  | [optional] 
**available** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

