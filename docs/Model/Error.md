# Error

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**errorType** | **string** |  | [optional] 
**errorCode** | **string** |  | [optional] 
**errorMessage** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

