# JobResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional] 
**status** | **string** |  | [optional] 
**request** | [**\Everpress\Rutter\Model\JobResponseRequest**](JobResponseRequest.md) |  | [optional] 
**response** | [**\Everpress\Rutter\Model\JobResponseResponse**](JobResponseResponse.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

