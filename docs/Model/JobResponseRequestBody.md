# JobResponseRequestBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**product** | [**\Everpress\Rutter\Model\Product**](Product.md) |  | [optional] 
**accessToken** | **string** |  | [optional] 
**responseMode** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

