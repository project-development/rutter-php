# OrderIdFulfillmentsBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**fulfillment** | [**\Everpress\Rutter\Model\OrderFulfill**](OrderFulfill.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

