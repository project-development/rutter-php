# PublicTokenExchangeBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**clientId** | **string** |  | [optional] 
**secret** | **string** |  | [optional] 
**publicToken** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

