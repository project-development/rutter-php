# Everpress\Rutter{{classname}}

All URIs are relative to *https://production.rutterapi.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get**](StoreApi.md#get) | **GET** /store | Detail

# **get**
> \Everpress\Rutter\Model\StoreResponse get($accessToken)

Detail

Fetch the store associated with the connection

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
// Configure HTTP basic authorization: basicAuth
$config = Everpress\Rutter\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Everpress\Rutter\Api{{classname}}(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$accessToken = "accessToken_example"; // string | The access token of the store

try {
    $result = $apiInstance->get($accessToken);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StoreApi->get: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accessToken** | **string**| The access token of the store |

### Return type

[**\Everpress\Rutter\Model\StoreResponse**](../Model/StoreResponse.md)

### Authorization

[basicAuth](../../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

