# Everpress\Rutter{{classname}}

All URIs are relative to *https://production.rutterapi.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteConnection**](ConnectionsApi.md#deleteConnection) | **DELETE** /connections/{connectionId} | Delete a Connection
[**getCredential**](ConnectionsApi.md#getCredential) | **GET** /connections/credentials | Fetch Connection Credentials

# **deleteConnection**
> \Everpress\Rutter\Model\SuccessResponse deleteConnection($connectionId)

Delete a Connection

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
// Configure HTTP basic authorization: basicAuth
$config = Everpress\Rutter\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Everpress\Rutter\Api{{classname}}(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$connectionId = "connectionId_example"; // string | The id of the connection

try {
    $result = $apiInstance->deleteConnection($connectionId);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ConnectionsApi->deleteConnection: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **connectionId** | **string**| The id of the connection |

### Return type

[**\Everpress\Rutter\Model\SuccessResponse**](../Model/SuccessResponse.md)

### Authorization

[basicAuth](../../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getCredential**
> \Everpress\Rutter\Model\CredentialResponse getCredential($accessToken)

Fetch Connection Credentials

The Fetch Connection Credentials API allows you to fetch the underlying API keys and tokens that are used to make authenticated requests for each platform. Depending on the type of authentication used in the underlying platform, different fields may be returned. You should view the platform-specific API pages to properly format your request.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
// Configure HTTP basic authorization: basicAuth
$config = Everpress\Rutter\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Everpress\Rutter\Api{{classname}}(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$accessToken = "accessToken_example"; // string | The access token of the store

try {
    $result = $apiInstance->getCredential($accessToken);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ConnectionsApi->getCredential: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accessToken** | **string**| The access token of the store |

### Return type

[**\Everpress\Rutter\Model\CredentialResponse**](../Model/CredentialResponse.md)

### Authorization

[basicAuth](../../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

