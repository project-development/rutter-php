# Everpress\Rutter{{classname}}

All URIs are relative to *https://production.rutterapi.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createProduct**](ProductsApi.md#createProduct) | **POST** /products | Create
[**createProductV2**](ProductsApi.md#createProductV2) | **POST** /v2/products | Create a product async
[**deleteProduct**](ProductsApi.md#deleteProduct) | **DELETE** /products/{productId} | Delete
[**getJobById**](ProductsApi.md#getJobById) | **GET** /v2/jobs/{jobId} | Detail
[**getProductById**](ProductsApi.md#getProductById) | **GET** /products/{productId} | Detail
[**getProducts**](ProductsApi.md#getProducts) | **GET** /products | List

# **createProduct**
> \Everpress\Rutter\Model\ProductsBody createProduct($body, $accessToken)

Create

Creates a product in a merchant's store. A Rutter product must contain at least one variant, which is a specific configuration of the product & has an associated SKU. See Variants Array below for more information.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
// Configure HTTP basic authorization: basicAuth
$config = Everpress\Rutter\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Everpress\Rutter\Api{{classname}}(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \Everpress\Rutter\Model\ProductsBody(); // \Everpress\Rutter\Model\ProductsBody | 
$accessToken = "accessToken_example"; // string | The access token of the store

try {
    $result = $apiInstance->createProduct($body, $accessToken);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductsApi->createProduct: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Everpress\Rutter\Model\ProductsBody**](../Model/ProductsBody.md)|  |
 **accessToken** | **string**| The access token of the store |

### Return type

[**\Everpress\Rutter\Model\ProductsBody**](../Model/ProductsBody.md)

### Authorization

[basicAuth](../../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **createProductV2**
> \Everpress\Rutter\Model\AsyncResponse createProductV2($body, $idempotencyKey)

Create a product async

Creates a product in a merchant's store. A Rutter product must contain at least one variant, which is a specific configuration of the product & has an associated SKU. See Variants Array below for more information.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
// Configure HTTP basic authorization: basicAuth
$config = Everpress\Rutter\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Everpress\Rutter\Api{{classname}}(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \Everpress\Rutter\Model\V2ProductsBody(); // \Everpress\Rutter\Model\V2ProductsBody | 
$idempotencyKey = "idempotencyKey_example"; // string | 

try {
    $result = $apiInstance->createProductV2($body, $idempotencyKey);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductsApi->createProductV2: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Everpress\Rutter\Model\V2ProductsBody**](../Model/V2ProductsBody.md)|  |
 **idempotencyKey** | **string**|  | [optional]

### Return type

[**\Everpress\Rutter\Model\AsyncResponse**](../Model/AsyncResponse.md)

### Authorization

[basicAuth](../../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteProduct**
> \Everpress\Rutter\Model\DeleteProductResponse deleteProduct($accessToken, $productId)

Delete

Delete a product

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
// Configure HTTP basic authorization: basicAuth
$config = Everpress\Rutter\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Everpress\Rutter\Api{{classname}}(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$accessToken = "accessToken_example"; // string | The access token of the store
$productId = "productId_example"; // string | The id of the product to retrieve

try {
    $result = $apiInstance->deleteProduct($accessToken, $productId);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductsApi->deleteProduct: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accessToken** | **string**| The access token of the store |
 **productId** | **string**| The id of the product to retrieve |

### Return type

[**\Everpress\Rutter\Model\DeleteProductResponse**](../Model/DeleteProductResponse.md)

### Authorization

[basicAuth](../../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getJobById**
> \Everpress\Rutter\Model\JobResponse getJobById($accessToken, $jobId)

Detail

Fetch a job

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
// Configure HTTP basic authorization: basicAuth
$config = Everpress\Rutter\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Everpress\Rutter\Api{{classname}}(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$accessToken = "accessToken_example"; // string | The access token of the store
$jobId = "jobId_example"; // string | The id of the job to retrieve

try {
    $result = $apiInstance->getJobById($accessToken, $jobId);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductsApi->getJobById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accessToken** | **string**| The access token of the store |
 **jobId** | **string**| The id of the job to retrieve |

### Return type

[**\Everpress\Rutter\Model\JobResponse**](../Model/JobResponse.md)

### Authorization

[basicAuth](../../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getProductById**
> \Everpress\Rutter\Model\ProductResponse getProductById($accessToken, $productId)

Detail

Fetch a product

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
// Configure HTTP basic authorization: basicAuth
$config = Everpress\Rutter\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Everpress\Rutter\Api{{classname}}(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$accessToken = "accessToken_example"; // string | The access token of the store
$productId = "productId_example"; // string | The id of the product to retrieve

try {
    $result = $apiInstance->getProductById($accessToken, $productId);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductsApi->getProductById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accessToken** | **string**| The access token of the store |
 **productId** | **string**| The id of the product to retrieve |

### Return type

[**\Everpress\Rutter\Model\ProductResponse**](../Model/ProductResponse.md)

### Authorization

[basicAuth](../../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getProducts**
> \Everpress\Rutter\Model\ProductsResponse getProducts($accessToken, $limit, $cursor, $createdAtMin, $createdAtMax, $ids)

List

Fetch All Products

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
// Configure HTTP basic authorization: basicAuth
$config = Everpress\Rutter\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Everpress\Rutter\Api{{classname}}(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$accessToken = "accessToken_example"; // string | The access token of the store
$limit = 56; // int | The number of records to fetch (default and max=50)
$cursor = "cursor_example"; // string | A cursor for use in pagination.
$createdAtMin = 56; // int | Show products created at or after date, formatted as a UNIX timestamp in milliseconds.
$createdAtMax = 56; // int | Show products updated at or after date, formatted as a UNIX timestamp in milliseconds.
$ids = array("ids_example"); // string[] | Return only products specified by a comma-separated list of product IDs.

try {
    $result = $apiInstance->getProducts($accessToken, $limit, $cursor, $createdAtMin, $createdAtMax, $ids);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductsApi->getProducts: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accessToken** | **string**| The access token of the store |
 **limit** | **int**| The number of records to fetch (default and max&#x3D;50) | [optional]
 **cursor** | **string**| A cursor for use in pagination. | [optional]
 **createdAtMin** | **int**| Show products created at or after date, formatted as a UNIX timestamp in milliseconds. | [optional]
 **createdAtMax** | **int**| Show products updated at or after date, formatted as a UNIX timestamp in milliseconds. | [optional]
 **ids** | [**string[]**](../Model/string.md)| Return only products specified by a comma-separated list of product IDs. | [optional]

### Return type

[**\Everpress\Rutter\Model\ProductsResponse**](../Model/ProductsResponse.md)

### Authorization

[basicAuth](../../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

