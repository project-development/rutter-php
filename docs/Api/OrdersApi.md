# Everpress\Rutter{{classname}}

All URIs are relative to *https://production.rutterapi.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**fulfillOrder**](OrdersApi.md#fulfillOrder) | **POST** /orders/{orderId}/fulfillments | Fulfill an Order
[**getOrderById**](OrdersApi.md#getOrderById) | **GET** /orders/{orderId} | Detail
[**getOrders**](OrdersApi.md#getOrders) | **GET** /orders | List

# **fulfillOrder**
> \Everpress\Rutter\Model\InlineResponse200 fulfillOrder($body, $accessToken, $orderId)

Fulfill an Order

After you create a fulfillment for an Order, Rutter automatically updates the fulfillment_status of the order to fulfilled both internally and in the Ecommerce platform.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
// Configure HTTP basic authorization: basicAuth
$config = Everpress\Rutter\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Everpress\Rutter\Api{{classname}}(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \Everpress\Rutter\Model\OrderIdFulfillmentsBody(); // \Everpress\Rutter\Model\OrderIdFulfillmentsBody | 
$accessToken = "accessToken_example"; // string | The access token of the store
$orderId = "orderId_example"; // string | The id of the order to retrieve

try {
    $result = $apiInstance->fulfillOrder($body, $accessToken, $orderId);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OrdersApi->fulfillOrder: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Everpress\Rutter\Model\OrderIdFulfillmentsBody**](../Model/OrderIdFulfillmentsBody.md)|  |
 **accessToken** | **string**| The access token of the store |
 **orderId** | **string**| The id of the order to retrieve |

### Return type

[**\Everpress\Rutter\Model\InlineResponse200**](../Model/InlineResponse200.md)

### Authorization

[basicAuth](../../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getOrderById**
> \Everpress\Rutter\Model\OrderResponse getOrderById($accessToken, $orderId)

Detail

Fetch and order

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
// Configure HTTP basic authorization: basicAuth
$config = Everpress\Rutter\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Everpress\Rutter\Api{{classname}}(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$accessToken = "accessToken_example"; // string | The access token of the store
$orderId = "orderId_example"; // string | The id of the order to retrieve

try {
    $result = $apiInstance->getOrderById($accessToken, $orderId);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OrdersApi->getOrderById: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accessToken** | **string**| The access token of the store |
 **orderId** | **string**| The id of the order to retrieve |

### Return type

[**\Everpress\Rutter\Model\OrderResponse**](../Model/OrderResponse.md)

### Authorization

[basicAuth](../../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getOrders**
> \Everpress\Rutter\Model\OrdersResponse getOrders($accessToken, $limit, $cursor, $fulfillmentStatus, $paymentStatus, $createdAtMin, $createdAtMax, $updatedAtMin, $updatedAtMax, $orderNumber, $properties, $expand)

List

Fetch All Orders

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
// Configure HTTP basic authorization: basicAuth
$config = Everpress\Rutter\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Everpress\Rutter\Api{{classname}}(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$accessToken = "accessToken_example"; // string | The access token of the store
$limit = 56; // int | The number of records to fetch (default and max=50)
$cursor = "cursor_example"; // string | A cursor for use in pagination.
$fulfillmentStatus = "fulfillmentStatus_example"; // string | Filter orders by their fulfillment_status (fulfilled, unfulfilled, partial, any)
$paymentStatus = "paymentStatus_example"; // string | Filters orders by payment_status. One of (paid, pending, or refunded)
$createdAtMin = 789; // int | Show orders created at or after date, formatted as a UNIX timestamp in milliseconds.
$createdAtMax = 789; // int | Show orders updated at or after date, formatted as a UNIX timestamp in milliseconds.
$updatedAtMin = 789; // int | Show orders created at or after date, formatted as a UNIX timestamp in milliseconds.
$updatedAtMax = 789; // int | Show orders created at or before date, formatted as a UNIX timestamp in milliseconds.
$orderNumber = 789; // int | Exact order number for an order
$properties = "properties_example"; // string | Return only certain fields specified by a comma-separated list of field names. E.g. id,status
$expand = "expand_example"; // string | See https://docs.rutterapi.com/reference#orders-expansions

try {
    $result = $apiInstance->getOrders($accessToken, $limit, $cursor, $fulfillmentStatus, $paymentStatus, $createdAtMin, $createdAtMax, $updatedAtMin, $updatedAtMax, $orderNumber, $properties, $expand);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling OrdersApi->getOrders: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accessToken** | **string**| The access token of the store |
 **limit** | **int**| The number of records to fetch (default and max&#x3D;50) | [optional]
 **cursor** | **string**| A cursor for use in pagination. | [optional]
 **fulfillmentStatus** | **string**| Filter orders by their fulfillment_status (fulfilled, unfulfilled, partial, any) | [optional]
 **paymentStatus** | **string**| Filters orders by payment_status. One of (paid, pending, or refunded) | [optional]
 **createdAtMin** | **int**| Show orders created at or after date, formatted as a UNIX timestamp in milliseconds. | [optional]
 **createdAtMax** | **int**| Show orders updated at or after date, formatted as a UNIX timestamp in milliseconds. | [optional]
 **updatedAtMin** | **int**| Show orders created at or after date, formatted as a UNIX timestamp in milliseconds. | [optional]
 **updatedAtMax** | **int**| Show orders created at or before date, formatted as a UNIX timestamp in milliseconds. | [optional]
 **orderNumber** | **int**| Exact order number for an order | [optional]
 **properties** | **string**| Return only certain fields specified by a comma-separated list of field names. E.g. id,status | [optional]
 **expand** | **string**| See https://docs.rutterapi.com/reference#orders-expansions | [optional]

### Return type

[**\Everpress\Rutter\Model\OrdersResponse**](../Model/OrdersResponse.md)

### Authorization

[basicAuth](../../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

