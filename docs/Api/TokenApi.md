# Everpress\Rutter{{classname}}

All URIs are relative to *https://production.rutterapi.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**exchangeToken**](TokenApi.md#exchangeToken) | **POST** /item/public_token/exchange | Exchange Tokens

# **exchangeToken**
> \Everpress\Rutter\Model\ExchangeTokenResponse exchangeToken($body)

Exchange Tokens

Exchange public token for an access token

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Everpress\Rutter\Api{{classname}}(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Everpress\Rutter\Model\PublicTokenExchangeBody(); // \Everpress\Rutter\Model\PublicTokenExchangeBody | 

try {
    $result = $apiInstance->exchangeToken($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling TokenApi->exchangeToken: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Everpress\Rutter\Model\PublicTokenExchangeBody**](../Model/PublicTokenExchangeBody.md)|  |

### Return type

[**\Everpress\Rutter\Model\ExchangeTokenResponse**](../Model/ExchangeTokenResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

