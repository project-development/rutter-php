# Everpress\Rutter{{classname}}

All URIs are relative to *https://production.rutterapi.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**fulfillOrder**](FulfillmentsApi.md#fulfillOrder) | **POST** /orders/{orderId}/fulfillments | Fulfill an Order

# **fulfillOrder**
> \Everpress\Rutter\Model\InlineResponse200 fulfillOrder($body, $accessToken, $orderId)

Fulfill an Order

After you create a fulfillment for an Order, Rutter automatically updates the fulfillment_status of the order to fulfilled both internally and in the Ecommerce platform.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
// Configure HTTP basic authorization: basicAuth
$config = Everpress\Rutter\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Everpress\Rutter\Api{{classname}}(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \Everpress\Rutter\Model\OrderIdFulfillmentsBody(); // \Everpress\Rutter\Model\OrderIdFulfillmentsBody | 
$accessToken = "accessToken_example"; // string | The access token of the store
$orderId = "orderId_example"; // string | The id of the order to retrieve

try {
    $result = $apiInstance->fulfillOrder($body, $accessToken, $orderId);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling FulfillmentsApi->fulfillOrder: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Everpress\Rutter\Model\OrderIdFulfillmentsBody**](../Model/OrderIdFulfillmentsBody.md)|  |
 **accessToken** | **string**| The access token of the store |
 **orderId** | **string**| The id of the order to retrieve |

### Return type

[**\Everpress\Rutter\Model\InlineResponse200**](../Model/InlineResponse200.md)

### Authorization

[basicAuth](../../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

