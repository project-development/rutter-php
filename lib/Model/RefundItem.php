<?php
/**
 * RefundItem
 *
 * PHP version 7
 *
 * @category Class
 * @package  Everpress\Rutter
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Everpress Rutter API Wrapper
 *
 * Everpress Rutter API Wrapper
 *
 * OpenAPI spec version: 1.0.0
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 3.0.34
 */
/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Everpress\Rutter\Model;

use \ArrayAccess;
use \Everpress\Rutter\ObjectSerializer;

/**
 * RefundItem Class Doc Comment
 *
 * @category Class
 * @package  Everpress\Rutter
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class RefundItem implements ModelInterface, ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
    * The original name of the model.
    */
    protected static string $swaggerModelName = 'RefundItem';

    /**
    * Array of property to type mappings. Used for (de)serialization
    *
    * @var string[]
    */
    protected static array $swaggerTypes = [
        'lineItemId' => 'string',
'quantity' => 'int',
'amount' => 'string'    ];

    /**
    * Array of property to format mappings. Used for (de)serialization
    *
    * @var string[]
    */
    protected static array $swaggerFormats = [
        'lineItemId' => null,
'quantity' => 'int32',
'amount' => null    ];

    /**
    * Array of property to type mappings. Used for (de)serialization
    */
    public static function swaggerTypes(): array
    {
        return self::$swaggerTypes;
    }

    /**
    * Array of property to format mappings. Used for (de)serialization
    */
    public static function swaggerFormats(): array
    {
        return self::$swaggerFormats;
    }

    /**
    * Array of attributes where the key is the local name,
    * and the value is the original name
    *
    * @var string[]
    */
    protected static array $attributeMap = [
        'lineItemId' => 'line_item_id',
'quantity' => 'quantity',
'amount' => 'amount'    ];

    /**
    * Array of attributes to setter functions (for deserialization of responses)
    *
    * @var string[]
    */
    protected static array $setters = [
        'lineItemId' => 'setLineItemId',
'quantity' => 'setQuantity',
'amount' => 'setAmount'    ];

    /**
    * Array of attributes to getter functions (for serialization of requests)
    *
    * @var string[]
    */
    protected static array $getters = [
        'lineItemId' => 'getLineItemId',
'quantity' => 'getQuantity',
'amount' => 'getAmount'    ];

    /**
    * Array of attributes where the key is the local name,
    * and the value is the original name
    */
    public static function attributeMap(): array
    {
        return self::$attributeMap;
    }

    /**
    * Array of attributes to setter functions (for deserialization of responses)
    */
    public static function setters(): array
    {
        return self::$setters;
    }

    /**
    * Array of attributes to getter functions (for serialization of requests)
    */
    public static function getters(): array
    {
        return self::$getters;
    }

    /**
    * The original name of the model.
    */
    public function getModelName(): string
    {
        return self::$swaggerModelName;
    }

    

    /**
    * Associative array for storing property values
    *
    * @var mixed[]
    */
    protected $container = [];

    /**
    * Constructor
    *
    * @param mixed[] $data Associated array of property values
    *                      initializing the model
    */
    public function __construct(array $data = null)
    {
        $this->container['lineItemId'] = isset($data['lineItemId']) ? $data['lineItemId'] : null;
        $this->container['quantity'] = isset($data['quantity']) ? $data['quantity'] : null;
        $this->container['amount'] = isset($data['amount']) ? $data['amount'] : null;
    }

    /**
    * Show all the invalid properties with reasons.
    *
    * @return array invalid properties with reasons
    */
    public function listInvalidProperties(): array
    {
        $invalidProperties = [];

        return $invalidProperties;
    }

    /**
    * Validate all the properties in the model
    * return true if all passed
    *
    * @return bool True if all properties are valid
    */
    public function valid(): bool
    {
        return count($this->listInvalidProperties()) === 0;
    }

    /**
    * Gets lineItemId
    *
    */
    public function getLineItemId(): ?string
    {
        return $this->container['lineItemId'];
    }

    /**
    * Sets lineItemId
    *
    * @param string $lineItemId lineItemId
    */
    public function setLineItemId(string $lineItemId): self
    {
        $this->container['lineItemId'] = $lineItemId;

        return $this;
    }
    /**
    * Gets quantity
    *
    */
    public function getQuantity(): ?int
    {
        return $this->container['quantity'];
    }

    /**
    * Sets quantity
    *
    * @param int $quantity quantity
    */
    public function setQuantity(int $quantity): self
    {
        $this->container['quantity'] = $quantity;

        return $this;
    }
    /**
    * Gets amount
    *
    */
    public function getAmount(): ?string
    {
        return $this->container['amount'];
    }

    /**
    * Sets amount
    *
    * @param string $amount amount
    */
    public function setAmount(string $amount): self
    {
        $this->container['amount'] = $amount;

        return $this;
    }

    /**
    * Returns true if offset exists. False otherwise.
    *
    * @param integer $offset Offset
    *
    * @return boolean
    */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
    * Gets offset.
    *
    * @param integer $offset Offset
    *
    * @return mixed
    */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
    * Sets value based on offset.
    *
    * @param integer $offset Offset
    * @param mixed   $value  Value to be set
    *
    * @return void
    */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
    * Unsets offset.
    *
    * @param integer $offset Offset
    *
    * @return void
    */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
    * Gets the string presentation of the object
    */
    public function __toString(): string
    {
    if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
        return json_encode(
        ObjectSerializer::sanitizeForSerialization($this),
        JSON_PRETTY_PRINT
    );
    }

    return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}
