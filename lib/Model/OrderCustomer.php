<?php
/**
 * OrderCustomer
 *
 * PHP version 7
 *
 * @category Class
 * @package  Everpress\Rutter
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Everpress Rutter API Wrapper
 *
 * Everpress Rutter API Wrapper
 *
 * OpenAPI spec version: 1.0.0
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 3.0.34
 */
/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Everpress\Rutter\Model;

use \ArrayAccess;
use \Everpress\Rutter\ObjectSerializer;

/**
 * OrderCustomer Class Doc Comment
 *
 * @category Class
 * @package  Everpress\Rutter
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class OrderCustomer implements ModelInterface, ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
    * The original name of the model.
    */
    protected static string $swaggerModelName = 'Order_customer';

    /**
    * Array of property to type mappings. Used for (de)serialization
    *
    * @var string[]
    */
    protected static array $swaggerTypes = [
        'id' => 'string',
'email' => 'string',
'firstName' => 'string',
'lastName' => 'string',
'ordersCount' => 'string',
'verifiedEmail' => 'bool',
'phone' => 'string',
'tags' => 'string'    ];

    /**
    * Array of property to format mappings. Used for (de)serialization
    *
    * @var string[]
    */
    protected static array $swaggerFormats = [
        'id' => null,
'email' => null,
'firstName' => null,
'lastName' => null,
'ordersCount' => null,
'verifiedEmail' => null,
'phone' => null,
'tags' => null    ];

    /**
    * Array of property to type mappings. Used for (de)serialization
    */
    public static function swaggerTypes(): array
    {
        return self::$swaggerTypes;
    }

    /**
    * Array of property to format mappings. Used for (de)serialization
    */
    public static function swaggerFormats(): array
    {
        return self::$swaggerFormats;
    }

    /**
    * Array of attributes where the key is the local name,
    * and the value is the original name
    *
    * @var string[]
    */
    protected static array $attributeMap = [
        'id' => 'id',
'email' => 'email',
'firstName' => 'first_name',
'lastName' => 'last_name',
'ordersCount' => 'orders_count',
'verifiedEmail' => 'verified_email',
'phone' => 'phone',
'tags' => 'tags'    ];

    /**
    * Array of attributes to setter functions (for deserialization of responses)
    *
    * @var string[]
    */
    protected static array $setters = [
        'id' => 'setId',
'email' => 'setEmail',
'firstName' => 'setFirstName',
'lastName' => 'setLastName',
'ordersCount' => 'setOrdersCount',
'verifiedEmail' => 'setVerifiedEmail',
'phone' => 'setPhone',
'tags' => 'setTags'    ];

    /**
    * Array of attributes to getter functions (for serialization of requests)
    *
    * @var string[]
    */
    protected static array $getters = [
        'id' => 'getId',
'email' => 'getEmail',
'firstName' => 'getFirstName',
'lastName' => 'getLastName',
'ordersCount' => 'getOrdersCount',
'verifiedEmail' => 'getVerifiedEmail',
'phone' => 'getPhone',
'tags' => 'getTags'    ];

    /**
    * Array of attributes where the key is the local name,
    * and the value is the original name
    */
    public static function attributeMap(): array
    {
        return self::$attributeMap;
    }

    /**
    * Array of attributes to setter functions (for deserialization of responses)
    */
    public static function setters(): array
    {
        return self::$setters;
    }

    /**
    * Array of attributes to getter functions (for serialization of requests)
    */
    public static function getters(): array
    {
        return self::$getters;
    }

    /**
    * The original name of the model.
    */
    public function getModelName(): string
    {
        return self::$swaggerModelName;
    }

    

    /**
    * Associative array for storing property values
    *
    * @var mixed[]
    */
    protected $container = [];

    /**
    * Constructor
    *
    * @param mixed[] $data Associated array of property values
    *                      initializing the model
    */
    public function __construct(array $data = null)
    {
        $this->container['id'] = isset($data['id']) ? $data['id'] : null;
        $this->container['email'] = isset($data['email']) ? $data['email'] : null;
        $this->container['firstName'] = isset($data['firstName']) ? $data['firstName'] : null;
        $this->container['lastName'] = isset($data['lastName']) ? $data['lastName'] : null;
        $this->container['ordersCount'] = isset($data['ordersCount']) ? $data['ordersCount'] : null;
        $this->container['verifiedEmail'] = isset($data['verifiedEmail']) ? $data['verifiedEmail'] : null;
        $this->container['phone'] = isset($data['phone']) ? $data['phone'] : null;
        $this->container['tags'] = isset($data['tags']) ? $data['tags'] : null;
    }

    /**
    * Show all the invalid properties with reasons.
    *
    * @return array invalid properties with reasons
    */
    public function listInvalidProperties(): array
    {
        $invalidProperties = [];

        return $invalidProperties;
    }

    /**
    * Validate all the properties in the model
    * return true if all passed
    *
    * @return bool True if all properties are valid
    */
    public function valid(): bool
    {
        return count($this->listInvalidProperties()) === 0;
    }

    /**
    * Gets id
    *
    */
    public function getId(): ?string
    {
        return $this->container['id'];
    }

    /**
    * Sets id
    *
    * @param string $id id
    */
    public function setId(string $id): self
    {
        $this->container['id'] = $id;

        return $this;
    }
    /**
    * Gets email
    *
    */
    public function getEmail(): ?string
    {
        return $this->container['email'];
    }

    /**
    * Sets email
    *
    * @param string $email email
    */
    public function setEmail(string $email): self
    {
        $this->container['email'] = $email;

        return $this;
    }
    /**
    * Gets firstName
    *
    */
    public function getFirstName(): ?string
    {
        return $this->container['firstName'];
    }

    /**
    * Sets firstName
    *
    * @param string $firstName firstName
    */
    public function setFirstName(string $firstName): self
    {
        $this->container['firstName'] = $firstName;

        return $this;
    }
    /**
    * Gets lastName
    *
    */
    public function getLastName(): ?string
    {
        return $this->container['lastName'];
    }

    /**
    * Sets lastName
    *
    * @param string $lastName lastName
    */
    public function setLastName(string $lastName): self
    {
        $this->container['lastName'] = $lastName;

        return $this;
    }
    /**
    * Gets ordersCount
    *
    */
    public function getOrdersCount(): ?string
    {
        return $this->container['ordersCount'];
    }

    /**
    * Sets ordersCount
    *
    * @param string $ordersCount ordersCount
    */
    public function setOrdersCount(string $ordersCount): self
    {
        $this->container['ordersCount'] = $ordersCount;

        return $this;
    }
    /**
    * Gets verifiedEmail
    *
    */
    public function getVerifiedEmail(): ?bool
    {
        return $this->container['verifiedEmail'];
    }

    /**
    * Sets verifiedEmail
    *
    * @param bool $verifiedEmail verifiedEmail
    */
    public function setVerifiedEmail(bool $verifiedEmail): self
    {
        $this->container['verifiedEmail'] = $verifiedEmail;

        return $this;
    }
    /**
    * Gets phone
    *
    */
    public function getPhone(): ?string
    {
        return $this->container['phone'];
    }

    /**
    * Sets phone
    *
    * @param string $phone phone
    */
    public function setPhone(string $phone): self
    {
        $this->container['phone'] = $phone;

        return $this;
    }
    /**
    * Gets tags
    *
    */
    public function getTags(): ?string
    {
        return $this->container['tags'];
    }

    /**
    * Sets tags
    *
    * @param string $tags tags
    */
    public function setTags(string $tags): self
    {
        $this->container['tags'] = $tags;

        return $this;
    }

    /**
    * Returns true if offset exists. False otherwise.
    *
    * @param integer $offset Offset
    *
    * @return boolean
    */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
    * Gets offset.
    *
    * @param integer $offset Offset
    *
    * @return mixed
    */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
    * Sets value based on offset.
    *
    * @param integer $offset Offset
    * @param mixed   $value  Value to be set
    *
    * @return void
    */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
    * Unsets offset.
    *
    * @param integer $offset Offset
    *
    * @return void
    */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
    * Gets the string presentation of the object
    */
    public function __toString(): string
    {
    if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
        return json_encode(
        ObjectSerializer::sanitizeForSerialization($this),
        JSON_PRETTY_PRINT
    );
    }

    return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}
