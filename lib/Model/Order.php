<?php
/**
 * Order
 *
 * PHP version 7
 *
 * @category Class
 * @package  Everpress\Rutter
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Everpress Rutter API Wrapper
 *
 * Everpress Rutter API Wrapper
 *
 * OpenAPI spec version: 1.0.0
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 3.0.34
 */
/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Everpress\Rutter\Model;

use \ArrayAccess;
use \Everpress\Rutter\ObjectSerializer;

/**
 * Order Class Doc Comment
 *
 * @category Class
 * @package  Everpress\Rutter
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class Order implements ModelInterface, ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
    * The original name of the model.
    */
    protected static string $swaggerModelName = 'Order';

    /**
    * Array of property to type mappings. Used for (de)serialization
    *
    * @var string[]
    */
    protected static array $swaggerTypes = [
        'id' => 'string',
'platformId' => 'string',
'orderNumber' => 'string',
'status' => 'string',
'paymentStatus' => 'string',
'fulfillmentStatus' => 'string',
'fulfillments' => '\Everpress\Rutter\Model\OrderFulfillments[]',
'lineItems' => '\Everpress\Rutter\Model\LineItem[]',
'refunds' => '\Everpress\Rutter\Model\OrderRefunds[]',
'billingAddress' => '\Everpress\Rutter\Model\Address',
'shippingAddress' => '\Everpress\Rutter\Model\Address',
'customer' => '\Everpress\Rutter\Model\OrderCustomer',
'totalShipping' => 'float',
'totalDiscount' => 'int',
'totalTax' => 'float',
'totalPrice' => 'float',
'isoCurrencyCode' => 'string',
'createdAt' => '\DateTime',
'updatedAt' => '\DateTime'    ];

    /**
    * Array of property to format mappings. Used for (de)serialization
    *
    * @var string[]
    */
    protected static array $swaggerFormats = [
        'id' => null,
'platformId' => null,
'orderNumber' => null,
'status' => null,
'paymentStatus' => null,
'fulfillmentStatus' => null,
'fulfillments' => null,
'lineItems' => null,
'refunds' => null,
'billingAddress' => null,
'shippingAddress' => null,
'customer' => null,
'totalShipping' => null,
'totalDiscount' => 'int32',
'totalTax' => null,
'totalPrice' => null,
'isoCurrencyCode' => null,
'createdAt' => 'date',
'updatedAt' => 'date'    ];

    /**
    * Array of property to type mappings. Used for (de)serialization
    */
    public static function swaggerTypes(): array
    {
        return self::$swaggerTypes;
    }

    /**
    * Array of property to format mappings. Used for (de)serialization
    */
    public static function swaggerFormats(): array
    {
        return self::$swaggerFormats;
    }

    /**
    * Array of attributes where the key is the local name,
    * and the value is the original name
    *
    * @var string[]
    */
    protected static array $attributeMap = [
        'id' => 'id',
'platformId' => 'platform_id',
'orderNumber' => 'order_number',
'status' => 'status',
'paymentStatus' => 'payment_status',
'fulfillmentStatus' => 'fulfillment_status',
'fulfillments' => 'fulfillments',
'lineItems' => 'line_items',
'refunds' => 'refunds',
'billingAddress' => 'billing_address',
'shippingAddress' => 'shipping_address',
'customer' => 'customer',
'totalShipping' => 'total_shipping',
'totalDiscount' => 'total_discount',
'totalTax' => 'total_tax',
'totalPrice' => 'total_price',
'isoCurrencyCode' => 'iso_currency_code',
'createdAt' => 'created_at',
'updatedAt' => 'updated_at'    ];

    /**
    * Array of attributes to setter functions (for deserialization of responses)
    *
    * @var string[]
    */
    protected static array $setters = [
        'id' => 'setId',
'platformId' => 'setPlatformId',
'orderNumber' => 'setOrderNumber',
'status' => 'setStatus',
'paymentStatus' => 'setPaymentStatus',
'fulfillmentStatus' => 'setFulfillmentStatus',
'fulfillments' => 'setFulfillments',
'lineItems' => 'setLineItems',
'refunds' => 'setRefunds',
'billingAddress' => 'setBillingAddress',
'shippingAddress' => 'setShippingAddress',
'customer' => 'setCustomer',
'totalShipping' => 'setTotalShipping',
'totalDiscount' => 'setTotalDiscount',
'totalTax' => 'setTotalTax',
'totalPrice' => 'setTotalPrice',
'isoCurrencyCode' => 'setIsoCurrencyCode',
'createdAt' => 'setCreatedAt',
'updatedAt' => 'setUpdatedAt'    ];

    /**
    * Array of attributes to getter functions (for serialization of requests)
    *
    * @var string[]
    */
    protected static array $getters = [
        'id' => 'getId',
'platformId' => 'getPlatformId',
'orderNumber' => 'getOrderNumber',
'status' => 'getStatus',
'paymentStatus' => 'getPaymentStatus',
'fulfillmentStatus' => 'getFulfillmentStatus',
'fulfillments' => 'getFulfillments',
'lineItems' => 'getLineItems',
'refunds' => 'getRefunds',
'billingAddress' => 'getBillingAddress',
'shippingAddress' => 'getShippingAddress',
'customer' => 'getCustomer',
'totalShipping' => 'getTotalShipping',
'totalDiscount' => 'getTotalDiscount',
'totalTax' => 'getTotalTax',
'totalPrice' => 'getTotalPrice',
'isoCurrencyCode' => 'getIsoCurrencyCode',
'createdAt' => 'getCreatedAt',
'updatedAt' => 'getUpdatedAt'    ];

    /**
    * Array of attributes where the key is the local name,
    * and the value is the original name
    */
    public static function attributeMap(): array
    {
        return self::$attributeMap;
    }

    /**
    * Array of attributes to setter functions (for deserialization of responses)
    */
    public static function setters(): array
    {
        return self::$setters;
    }

    /**
    * Array of attributes to getter functions (for serialization of requests)
    */
    public static function getters(): array
    {
        return self::$getters;
    }

    /**
    * The original name of the model.
    */
    public function getModelName(): string
    {
        return self::$swaggerModelName;
    }

    

    /**
    * Associative array for storing property values
    *
    * @var mixed[]
    */
    protected $container = [];

    /**
    * Constructor
    *
    * @param mixed[] $data Associated array of property values
    *                      initializing the model
    */
    public function __construct(array $data = null)
    {
        $this->container['id'] = isset($data['id']) ? $data['id'] : null;
        $this->container['platformId'] = isset($data['platformId']) ? $data['platformId'] : null;
        $this->container['orderNumber'] = isset($data['orderNumber']) ? $data['orderNumber'] : null;
        $this->container['status'] = isset($data['status']) ? $data['status'] : null;
        $this->container['paymentStatus'] = isset($data['paymentStatus']) ? $data['paymentStatus'] : null;
        $this->container['fulfillmentStatus'] = isset($data['fulfillmentStatus']) ? $data['fulfillmentStatus'] : null;
        $this->container['fulfillments'] = isset($data['fulfillments']) ? $data['fulfillments'] : null;
        $this->container['lineItems'] = isset($data['lineItems']) ? $data['lineItems'] : null;
        $this->container['refunds'] = isset($data['refunds']) ? $data['refunds'] : null;
        $this->container['billingAddress'] = isset($data['billingAddress']) ? $data['billingAddress'] : null;
        $this->container['shippingAddress'] = isset($data['shippingAddress']) ? $data['shippingAddress'] : null;
        $this->container['customer'] = isset($data['customer']) ? $data['customer'] : null;
        $this->container['totalShipping'] = isset($data['totalShipping']) ? $data['totalShipping'] : null;
        $this->container['totalDiscount'] = isset($data['totalDiscount']) ? $data['totalDiscount'] : null;
        $this->container['totalTax'] = isset($data['totalTax']) ? $data['totalTax'] : null;
        $this->container['totalPrice'] = isset($data['totalPrice']) ? $data['totalPrice'] : null;
        $this->container['isoCurrencyCode'] = isset($data['isoCurrencyCode']) ? $data['isoCurrencyCode'] : null;
        $this->container['createdAt'] = isset($data['createdAt']) ? $data['createdAt'] : null;
        $this->container['updatedAt'] = isset($data['updatedAt']) ? $data['updatedAt'] : null;
    }

    /**
    * Show all the invalid properties with reasons.
    *
    * @return array invalid properties with reasons
    */
    public function listInvalidProperties(): array
    {
        $invalidProperties = [];

        return $invalidProperties;
    }

    /**
    * Validate all the properties in the model
    * return true if all passed
    *
    * @return bool True if all properties are valid
    */
    public function valid(): bool
    {
        return count($this->listInvalidProperties()) === 0;
    }

    /**
    * Gets id
    *
    */
    public function getId(): ?string
    {
        return $this->container['id'];
    }

    /**
    * Sets id
    *
    * @param string $id id
    */
    public function setId(string $id): self
    {
        $this->container['id'] = $id;

        return $this;
    }
    /**
    * Gets platformId
    *
    */
    public function getPlatformId(): ?string
    {
        return $this->container['platformId'];
    }

    /**
    * Sets platformId
    *
    * @param string $platformId platformId
    */
    public function setPlatformId(string $platformId): self
    {
        $this->container['platformId'] = $platformId;

        return $this;
    }
    /**
    * Gets orderNumber
    *
    */
    public function getOrderNumber(): ?string
    {
        return $this->container['orderNumber'];
    }

    /**
    * Sets orderNumber
    *
    * @param string $orderNumber orderNumber
    */
    public function setOrderNumber(string $orderNumber): self
    {
        $this->container['orderNumber'] = $orderNumber;

        return $this;
    }
    /**
    * Gets status
    *
    */
    public function getStatus(): ?string
    {
        return $this->container['status'];
    }

    /**
    * Sets status
    *
    * @param string $status status
    */
    public function setStatus(string $status): self
    {
        $this->container['status'] = $status;

        return $this;
    }
    /**
    * Gets paymentStatus
    *
    */
    public function getPaymentStatus(): ?string
    {
        return $this->container['paymentStatus'];
    }

    /**
    * Sets paymentStatus
    *
    * @param string $paymentStatus paymentStatus
    */
    public function setPaymentStatus(string $paymentStatus): self
    {
        $this->container['paymentStatus'] = $paymentStatus;

        return $this;
    }
    /**
    * Gets fulfillmentStatus
    *
    */
    public function getFulfillmentStatus(): ?string
    {
        return $this->container['fulfillmentStatus'];
    }

    /**
    * Sets fulfillmentStatus
    *
    * @param string $fulfillmentStatus fulfillmentStatus
    */
    public function setFulfillmentStatus(string $fulfillmentStatus): self
    {
        $this->container['fulfillmentStatus'] = $fulfillmentStatus;

        return $this;
    }
    /**
    * Gets fulfillments
    * @return \Everpress\Rutter\Model\OrderFulfillments[]
    */
    public function getFulfillments(): ?array
    {
        return $this->container['fulfillments'];
    }

    /**
    * Sets fulfillments
    *
    * @param \Everpress\Rutter\Model\OrderFulfillments[] $fulfillments fulfillments
    */
    public function setFulfillments(array $fulfillments): self
    {
        $this->container['fulfillments'] = $fulfillments;

        return $this;
    }
    /**
    * Gets lineItems
    * @return \Everpress\Rutter\Model\LineItem[]
    */
    public function getLineItems(): ?array
    {
        return $this->container['lineItems'];
    }

    /**
    * Sets lineItems
    *
    * @param \Everpress\Rutter\Model\LineItem[] $lineItems lineItems
    */
    public function setLineItems(array $lineItems): self
    {
        $this->container['lineItems'] = $lineItems;

        return $this;
    }
    /**
    * Gets refunds
    * @return \Everpress\Rutter\Model\OrderRefunds[]
    */
    public function getRefunds(): ?array
    {
        return $this->container['refunds'];
    }

    /**
    * Sets refunds
    *
    * @param \Everpress\Rutter\Model\OrderRefunds[] $refunds refunds
    */
    public function setRefunds(array $refunds): self
    {
        $this->container['refunds'] = $refunds;

        return $this;
    }
    /**
    * Gets billingAddress
    *
    */
    public function getBillingAddress(): ?\Everpress\Rutter\Model\Address
    {
        return $this->container['billingAddress'];
    }

    /**
    * Sets billingAddress
    *
    * @param \Everpress\Rutter\Model\Address $billingAddress billingAddress
    */
    public function setBillingAddress(\Everpress\Rutter\Model\Address $billingAddress): self
    {
        $this->container['billingAddress'] = $billingAddress;

        return $this;
    }
    /**
    * Gets shippingAddress
    *
    */
    public function getShippingAddress(): ?\Everpress\Rutter\Model\Address
    {
        return $this->container['shippingAddress'];
    }

    /**
    * Sets shippingAddress
    *
    * @param \Everpress\Rutter\Model\Address $shippingAddress shippingAddress
    */
    public function setShippingAddress(\Everpress\Rutter\Model\Address $shippingAddress): self
    {
        $this->container['shippingAddress'] = $shippingAddress;

        return $this;
    }
    /**
    * Gets customer
    *
    */
    public function getCustomer(): ?\Everpress\Rutter\Model\OrderCustomer
    {
        return $this->container['customer'];
    }

    /**
    * Sets customer
    *
    * @param \Everpress\Rutter\Model\OrderCustomer $customer customer
    */
    public function setCustomer(\Everpress\Rutter\Model\OrderCustomer $customer): self
    {
        $this->container['customer'] = $customer;

        return $this;
    }
    /**
    * Gets totalShipping
    *
    */
    public function getTotalShipping(): ?float
    {
        return $this->container['totalShipping'];
    }

    /**
    * Sets totalShipping
    *
    * @param float $totalShipping totalShipping
    */
    public function setTotalShipping(float $totalShipping): self
    {
        $this->container['totalShipping'] = $totalShipping;

        return $this;
    }
    /**
    * Gets totalDiscount
    *
    */
    public function getTotalDiscount(): ?int
    {
        return $this->container['totalDiscount'];
    }

    /**
    * Sets totalDiscount
    *
    * @param int $totalDiscount totalDiscount
    */
    public function setTotalDiscount(int $totalDiscount): self
    {
        $this->container['totalDiscount'] = $totalDiscount;

        return $this;
    }
    /**
    * Gets totalTax
    *
    */
    public function getTotalTax(): ?float
    {
        return $this->container['totalTax'];
    }

    /**
    * Sets totalTax
    *
    * @param float $totalTax totalTax
    */
    public function setTotalTax(float $totalTax): self
    {
        $this->container['totalTax'] = $totalTax;

        return $this;
    }
    /**
    * Gets totalPrice
    *
    */
    public function getTotalPrice(): ?float
    {
        return $this->container['totalPrice'];
    }

    /**
    * Sets totalPrice
    *
    * @param float $totalPrice totalPrice
    */
    public function setTotalPrice(float $totalPrice): self
    {
        $this->container['totalPrice'] = $totalPrice;

        return $this;
    }
    /**
    * Gets isoCurrencyCode
    *
    */
    public function getIsoCurrencyCode(): ?string
    {
        return $this->container['isoCurrencyCode'];
    }

    /**
    * Sets isoCurrencyCode
    *
    * @param string $isoCurrencyCode isoCurrencyCode
    */
    public function setIsoCurrencyCode(string $isoCurrencyCode): self
    {
        $this->container['isoCurrencyCode'] = $isoCurrencyCode;

        return $this;
    }
    /**
    * Gets createdAt
    *
    */
    public function getCreatedAt(): ?\DateTime
    {
        return $this->container['createdAt'];
    }

    /**
    * Sets createdAt
    *
    * @param \DateTime $createdAt createdAt
    */
    public function setCreatedAt(\DateTime $createdAt): self
    {
        $this->container['createdAt'] = $createdAt;

        return $this;
    }
    /**
    * Gets updatedAt
    *
    */
    public function getUpdatedAt(): ?\DateTime
    {
        return $this->container['updatedAt'];
    }

    /**
    * Sets updatedAt
    *
    * @param \DateTime $updatedAt updatedAt
    */
    public function setUpdatedAt(\DateTime $updatedAt): self
    {
        $this->container['updatedAt'] = $updatedAt;

        return $this;
    }

    /**
    * Returns true if offset exists. False otherwise.
    *
    * @param integer $offset Offset
    *
    * @return boolean
    */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
    * Gets offset.
    *
    * @param integer $offset Offset
    *
    * @return mixed
    */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
    * Sets value based on offset.
    *
    * @param integer $offset Offset
    * @param mixed   $value  Value to be set
    *
    * @return void
    */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
    * Unsets offset.
    *
    * @param integer $offset Offset
    *
    * @return void
    */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
    * Gets the string presentation of the object
    */
    public function __toString(): string
    {
    if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
        return json_encode(
        ObjectSerializer::sanitizeForSerialization($this),
        JSON_PRETTY_PRINT
    );
    }

    return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}
