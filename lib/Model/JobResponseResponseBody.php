<?php
/**
 * JobResponseResponseBody
 *
 * PHP version 7
 *
 * @category Class
 * @package  Everpress\Rutter
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Everpress Rutter API Wrapper
 *
 * Everpress Rutter API Wrapper
 *
 * OpenAPI spec version: 1.0.0
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 3.0.34
 */
/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Everpress\Rutter\Model;

use \ArrayAccess;
use \Everpress\Rutter\ObjectSerializer;

/**
 * JobResponseResponseBody Class Doc Comment
 *
 * @category Class
 * @package  Everpress\Rutter
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class JobResponseResponseBody implements ModelInterface, ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
    * The original name of the model.
    */
    protected static string $swaggerModelName = 'JobResponse_response_body';

    /**
    * Array of property to type mappings. Used for (de)serialization
    *
    * @var string[]
    */
    protected static array $swaggerTypes = [
        'errors' => '\Everpress\Rutter\Model\AsyncError[]',
'product' => '\Everpress\Rutter\Model\Product'    ];

    /**
    * Array of property to format mappings. Used for (de)serialization
    *
    * @var string[]
    */
    protected static array $swaggerFormats = [
        'errors' => null,
'product' => null    ];

    /**
    * Array of property to type mappings. Used for (de)serialization
    */
    public static function swaggerTypes(): array
    {
        return self::$swaggerTypes;
    }

    /**
    * Array of property to format mappings. Used for (de)serialization
    */
    public static function swaggerFormats(): array
    {
        return self::$swaggerFormats;
    }

    /**
    * Array of attributes where the key is the local name,
    * and the value is the original name
    *
    * @var string[]
    */
    protected static array $attributeMap = [
        'errors' => 'errors',
'product' => 'product'    ];

    /**
    * Array of attributes to setter functions (for deserialization of responses)
    *
    * @var string[]
    */
    protected static array $setters = [
        'errors' => 'setErrors',
'product' => 'setProduct'    ];

    /**
    * Array of attributes to getter functions (for serialization of requests)
    *
    * @var string[]
    */
    protected static array $getters = [
        'errors' => 'getErrors',
'product' => 'getProduct'    ];

    /**
    * Array of attributes where the key is the local name,
    * and the value is the original name
    */
    public static function attributeMap(): array
    {
        return self::$attributeMap;
    }

    /**
    * Array of attributes to setter functions (for deserialization of responses)
    */
    public static function setters(): array
    {
        return self::$setters;
    }

    /**
    * Array of attributes to getter functions (for serialization of requests)
    */
    public static function getters(): array
    {
        return self::$getters;
    }

    /**
    * The original name of the model.
    */
    public function getModelName(): string
    {
        return self::$swaggerModelName;
    }

    

    /**
    * Associative array for storing property values
    *
    * @var mixed[]
    */
    protected $container = [];

    /**
    * Constructor
    *
    * @param mixed[] $data Associated array of property values
    *                      initializing the model
    */
    public function __construct(array $data = null)
    {
        $this->container['errors'] = isset($data['errors']) ? $data['errors'] : null;
        $this->container['product'] = isset($data['product']) ? $data['product'] : null;
    }

    /**
    * Show all the invalid properties with reasons.
    *
    * @return array invalid properties with reasons
    */
    public function listInvalidProperties(): array
    {
        $invalidProperties = [];

        return $invalidProperties;
    }

    /**
    * Validate all the properties in the model
    * return true if all passed
    *
    * @return bool True if all properties are valid
    */
    public function valid(): bool
    {
        return count($this->listInvalidProperties()) === 0;
    }

    /**
    * Gets errors
    * @return \Everpress\Rutter\Model\AsyncError[]
    */
    public function getErrors(): ?array
    {
        return $this->container['errors'];
    }

    /**
    * Sets errors
    *
    * @param \Everpress\Rutter\Model\AsyncError[] $errors errors
    */
    public function setErrors(array $errors): self
    {
        $this->container['errors'] = $errors;

        return $this;
    }
    /**
    * Gets product
    *
    */
    public function getProduct(): ?\Everpress\Rutter\Model\Product
    {
        return $this->container['product'];
    }

    /**
    * Sets product
    *
    * @param \Everpress\Rutter\Model\Product $product product
    */
    public function setProduct(\Everpress\Rutter\Model\Product $product): self
    {
        $this->container['product'] = $product;

        return $this;
    }

    /**
    * Returns true if offset exists. False otherwise.
    *
    * @param integer $offset Offset
    *
    * @return boolean
    */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
    * Gets offset.
    *
    * @param integer $offset Offset
    *
    * @return mixed
    */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
    * Sets value based on offset.
    *
    * @param integer $offset Offset
    * @param mixed   $value  Value to be set
    *
    * @return void
    */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
    * Unsets offset.
    *
    * @param integer $offset Offset
    *
    * @return void
    */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
    * Gets the string presentation of the object
    */
    public function __toString(): string
    {
    if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
        return json_encode(
        ObjectSerializer::sanitizeForSerialization($this),
        JSON_PRETTY_PRINT
    );
    }

    return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}
