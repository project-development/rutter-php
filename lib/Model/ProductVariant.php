<?php
/**
 * ProductVariant
 *
 * PHP version 7
 *
 * @category Class
 * @package  Everpress\Rutter
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Everpress Rutter API Wrapper
 *
 * Everpress Rutter API Wrapper
 *
 * OpenAPI spec version: 1.0.0
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 3.0.34
 */
/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Everpress\Rutter\Model;

use \ArrayAccess;
use \Everpress\Rutter\ObjectSerializer;

/**
 * ProductVariant Class Doc Comment
 *
 * @category Class
 * @package  Everpress\Rutter
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class ProductVariant implements ModelInterface, ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
    * The original name of the model.
    */
    protected static string $swaggerModelName = 'ProductVariant';

    /**
    * Array of property to type mappings. Used for (de)serialization
    *
    * @var string[]
    */
    protected static array $swaggerTypes = [
        'id' => 'string',
'productId' => 'string',
'title' => 'string',
'price' => 'int',
'isoCurrencyCode' => 'string',
'sku' => 'string',
'fulfillmentService' => 'string',
'inventoryManagement' => 'string',
'barcode' => 'string',
'unitCost' => 'string',
'weight' => '\Everpress\Rutter\Model\ProductVariantWeight',
'optionValues' => '\Everpress\Rutter\Model\ProductVariantOptionValues[]',
'images' => '\Everpress\Rutter\Model\ProductImages[]',
'inventory' => '\Everpress\Rutter\Model\ProductInventory',
'requiresShipping' => 'bool'    ];

    /**
    * Array of property to format mappings. Used for (de)serialization
    *
    * @var string[]
    */
    protected static array $swaggerFormats = [
        'id' => null,
'productId' => null,
'title' => null,
'price' => 'int32',
'isoCurrencyCode' => null,
'sku' => null,
'fulfillmentService' => null,
'inventoryManagement' => null,
'barcode' => null,
'unitCost' => 'nullable',
'weight' => null,
'optionValues' => null,
'images' => null,
'inventory' => null,
'requiresShipping' => null    ];

    /**
    * Array of property to type mappings. Used for (de)serialization
    */
    public static function swaggerTypes(): array
    {
        return self::$swaggerTypes;
    }

    /**
    * Array of property to format mappings. Used for (de)serialization
    */
    public static function swaggerFormats(): array
    {
        return self::$swaggerFormats;
    }

    /**
    * Array of attributes where the key is the local name,
    * and the value is the original name
    *
    * @var string[]
    */
    protected static array $attributeMap = [
        'id' => 'id',
'productId' => 'product_id',
'title' => 'title',
'price' => 'price',
'isoCurrencyCode' => 'iso_currency_code',
'sku' => 'sku',
'fulfillmentService' => 'fulfillment_service',
'inventoryManagement' => 'inventory_management',
'barcode' => 'barcode',
'unitCost' => 'unit_cost',
'weight' => 'weight',
'optionValues' => 'option_values',
'images' => 'images',
'inventory' => 'inventory',
'requiresShipping' => 'requires_shipping'    ];

    /**
    * Array of attributes to setter functions (for deserialization of responses)
    *
    * @var string[]
    */
    protected static array $setters = [
        'id' => 'setId',
'productId' => 'setProductId',
'title' => 'setTitle',
'price' => 'setPrice',
'isoCurrencyCode' => 'setIsoCurrencyCode',
'sku' => 'setSku',
'fulfillmentService' => 'setFulfillmentService',
'inventoryManagement' => 'setInventoryManagement',
'barcode' => 'setBarcode',
'unitCost' => 'setUnitCost',
'weight' => 'setWeight',
'optionValues' => 'setOptionValues',
'images' => 'setImages',
'inventory' => 'setInventory',
'requiresShipping' => 'setRequiresShipping'    ];

    /**
    * Array of attributes to getter functions (for serialization of requests)
    *
    * @var string[]
    */
    protected static array $getters = [
        'id' => 'getId',
'productId' => 'getProductId',
'title' => 'getTitle',
'price' => 'getPrice',
'isoCurrencyCode' => 'getIsoCurrencyCode',
'sku' => 'getSku',
'fulfillmentService' => 'getFulfillmentService',
'inventoryManagement' => 'getInventoryManagement',
'barcode' => 'getBarcode',
'unitCost' => 'getUnitCost',
'weight' => 'getWeight',
'optionValues' => 'getOptionValues',
'images' => 'getImages',
'inventory' => 'getInventory',
'requiresShipping' => 'getRequiresShipping'    ];

    /**
    * Array of attributes where the key is the local name,
    * and the value is the original name
    */
    public static function attributeMap(): array
    {
        return self::$attributeMap;
    }

    /**
    * Array of attributes to setter functions (for deserialization of responses)
    */
    public static function setters(): array
    {
        return self::$setters;
    }

    /**
    * Array of attributes to getter functions (for serialization of requests)
    */
    public static function getters(): array
    {
        return self::$getters;
    }

    /**
    * The original name of the model.
    */
    public function getModelName(): string
    {
        return self::$swaggerModelName;
    }

    

    /**
    * Associative array for storing property values
    *
    * @var mixed[]
    */
    protected $container = [];

    /**
    * Constructor
    *
    * @param mixed[] $data Associated array of property values
    *                      initializing the model
    */
    public function __construct(array $data = null)
    {
        $this->container['id'] = isset($data['id']) ? $data['id'] : null;
        $this->container['productId'] = isset($data['productId']) ? $data['productId'] : null;
        $this->container['title'] = isset($data['title']) ? $data['title'] : null;
        $this->container['price'] = isset($data['price']) ? $data['price'] : null;
        $this->container['isoCurrencyCode'] = isset($data['isoCurrencyCode']) ? $data['isoCurrencyCode'] : null;
        $this->container['sku'] = isset($data['sku']) ? $data['sku'] : null;
        $this->container['fulfillmentService'] = isset($data['fulfillmentService']) ? $data['fulfillmentService'] : null;
        $this->container['inventoryManagement'] = isset($data['inventoryManagement']) ? $data['inventoryManagement'] : null;
        $this->container['barcode'] = isset($data['barcode']) ? $data['barcode'] : null;
        $this->container['unitCost'] = isset($data['unitCost']) ? $data['unitCost'] : null;
        $this->container['weight'] = isset($data['weight']) ? $data['weight'] : null;
        $this->container['optionValues'] = isset($data['optionValues']) ? $data['optionValues'] : null;
        $this->container['images'] = isset($data['images']) ? $data['images'] : null;
        $this->container['inventory'] = isset($data['inventory']) ? $data['inventory'] : null;
        $this->container['requiresShipping'] = isset($data['requiresShipping']) ? $data['requiresShipping'] : null;
    }

    /**
    * Show all the invalid properties with reasons.
    *
    * @return array invalid properties with reasons
    */
    public function listInvalidProperties(): array
    {
        $invalidProperties = [];

        return $invalidProperties;
    }

    /**
    * Validate all the properties in the model
    * return true if all passed
    *
    * @return bool True if all properties are valid
    */
    public function valid(): bool
    {
        return count($this->listInvalidProperties()) === 0;
    }

    /**
    * Gets id
    *
    */
    public function getId(): ?string
    {
        return $this->container['id'];
    }

    /**
    * Sets id
    *
    * @param string $id id
    */
    public function setId(string $id): self
    {
        $this->container['id'] = $id;

        return $this;
    }
    /**
    * Gets productId
    *
    */
    public function getProductId(): ?string
    {
        return $this->container['productId'];
    }

    /**
    * Sets productId
    *
    * @param string $productId productId
    */
    public function setProductId(string $productId): self
    {
        $this->container['productId'] = $productId;

        return $this;
    }
    /**
    * Gets title
    *
    */
    public function getTitle(): ?string
    {
        return $this->container['title'];
    }

    /**
    * Sets title
    *
    * @param string $title title
    */
    public function setTitle(string $title): self
    {
        $this->container['title'] = $title;

        return $this;
    }
    /**
    * Gets price
    *
    */
    public function getPrice(): ?int
    {
        return $this->container['price'];
    }

    /**
    * Sets price
    *
    * @param int $price price
    */
    public function setPrice(int $price): self
    {
        $this->container['price'] = $price;

        return $this;
    }
    /**
    * Gets isoCurrencyCode
    *
    */
    public function getIsoCurrencyCode(): ?string
    {
        return $this->container['isoCurrencyCode'];
    }

    /**
    * Sets isoCurrencyCode
    *
    * @param string $isoCurrencyCode isoCurrencyCode
    */
    public function setIsoCurrencyCode(string $isoCurrencyCode): self
    {
        $this->container['isoCurrencyCode'] = $isoCurrencyCode;

        return $this;
    }
    /**
    * Gets sku
    *
    */
    public function getSku(): ?string
    {
        return $this->container['sku'];
    }

    /**
    * Sets sku
    *
    * @param string $sku sku
    */
    public function setSku(string $sku): self
    {
        $this->container['sku'] = $sku;

        return $this;
    }
    /**
    * Gets fulfillmentService
    *
    */
    public function getFulfillmentService(): ?string
    {
        return $this->container['fulfillmentService'];
    }

    /**
    * Sets fulfillmentService
    *
    * @param string $fulfillmentService fulfillmentService
    */
    public function setFulfillmentService(string $fulfillmentService): self
    {
        $this->container['fulfillmentService'] = $fulfillmentService;

        return $this;
    }
    /**
    * Gets inventoryManagement
    *
    */
    public function getInventoryManagement(): ?string
    {
        return $this->container['inventoryManagement'];
    }

    /**
    * Sets inventoryManagement
    *
    * @param string $inventoryManagement inventoryManagement
    */
    public function setInventoryManagement(string $inventoryManagement): self
    {
        $this->container['inventoryManagement'] = $inventoryManagement;

        return $this;
    }
    /**
    * Gets barcode
    *
    */
    public function getBarcode(): ?string
    {
        return $this->container['barcode'];
    }

    /**
    * Sets barcode
    *
    * @param string $barcode barcode
    */
    public function setBarcode(string $barcode): self
    {
        $this->container['barcode'] = $barcode;

        return $this;
    }
    /**
    * Gets unitCost
    *
    */
    public function getUnitCost(): ?string
    {
        return $this->container['unitCost'];
    }

    /**
    * Sets unitCost
    *
    * @param string $unitCost unitCost
    */
    public function setUnitCost(string $unitCost): self
    {
        $this->container['unitCost'] = $unitCost;

        return $this;
    }
    /**
    * Gets weight
    *
    */
    public function getWeight(): ?\Everpress\Rutter\Model\ProductVariantWeight
    {
        return $this->container['weight'];
    }

    /**
    * Sets weight
    *
    * @param \Everpress\Rutter\Model\ProductVariantWeight $weight weight
    */
    public function setWeight(\Everpress\Rutter\Model\ProductVariantWeight $weight): self
    {
        $this->container['weight'] = $weight;

        return $this;
    }
    /**
    * Gets optionValues
    * @return \Everpress\Rutter\Model\ProductVariantOptionValues[]
    */
    public function getOptionValues(): ?array
    {
        return $this->container['optionValues'];
    }

    /**
    * Sets optionValues
    *
    * @param \Everpress\Rutter\Model\ProductVariantOptionValues[] $optionValues optionValues
    */
    public function setOptionValues(array $optionValues): self
    {
        $this->container['optionValues'] = $optionValues;

        return $this;
    }
    /**
    * Gets images
    * @return \Everpress\Rutter\Model\ProductImages[]
    */
    public function getImages(): ?array
    {
        return $this->container['images'];
    }

    /**
    * Sets images
    *
    * @param \Everpress\Rutter\Model\ProductImages[] $images images
    */
    public function setImages(array $images): self
    {
        $this->container['images'] = $images;

        return $this;
    }
    /**
    * Gets inventory
    *
    */
    public function getInventory(): ?\Everpress\Rutter\Model\ProductInventory
    {
        return $this->container['inventory'];
    }

    /**
    * Sets inventory
    *
    * @param \Everpress\Rutter\Model\ProductInventory $inventory inventory
    */
    public function setInventory(\Everpress\Rutter\Model\ProductInventory $inventory): self
    {
        $this->container['inventory'] = $inventory;

        return $this;
    }
    /**
    * Gets requiresShipping
    *
    */
    public function getRequiresShipping(): ?bool
    {
        return $this->container['requiresShipping'];
    }

    /**
    * Sets requiresShipping
    *
    * @param bool $requiresShipping requiresShipping
    */
    public function setRequiresShipping(bool $requiresShipping): self
    {
        $this->container['requiresShipping'] = $requiresShipping;

        return $this;
    }

    /**
    * Returns true if offset exists. False otherwise.
    *
    * @param integer $offset Offset
    *
    * @return boolean
    */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
    * Gets offset.
    *
    * @param integer $offset Offset
    *
    * @return mixed
    */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
    * Sets value based on offset.
    *
    * @param integer $offset Offset
    * @param mixed   $value  Value to be set
    *
    * @return void
    */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
    * Unsets offset.
    *
    * @param integer $offset Offset
    *
    * @return void
    */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
    * Gets the string presentation of the object
    */
    public function __toString(): string
    {
    if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
        return json_encode(
        ObjectSerializer::sanitizeForSerialization($this),
        JSON_PRETTY_PRINT
    );
    }

    return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}
